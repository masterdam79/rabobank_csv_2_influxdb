import csv
import argparse
import sys
import requests
import datetime
import configparser

# Get some variables outside this script
config = configparser.ConfigParser()
config.read('./config.txt')
protocol = config['HOST']['PROTOCOL']
domain = config['HOST']['DOMAIN']
port = config['HOST']['PORT']
dbname = config['DATABASE']['NAME']
dbtag = config['DATABASE']['TAG']
csvamount = config['CSV']['AMOUNT']
csvbalance = config['CSV']['BALANCE']
csvparty = config['CSV']['PARTY']
csvdescription = config['CSV']['DESCRIPTION']


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

parser = MyParser()

# A file
parser.add_argument('-f', '--file', type=str, metavar='CSV File', required=True, help="The filename you wish to process")
parser.add_argument('-i', '--iban', type=str, metavar='IBAN/BBAN', required=True, help="The IBAN/BBAN you wish to process")

args = parser.parse_args()

with open(args.file) as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        date = row['Datum'].split('-')
        year = date[0]
        month = date[1]
        day = date[2]

        epochFromDate = (datetime.datetime(int(year),int(month),int(day),0,0) - datetime.datetime(1970,1,1)).total_seconds()
        epochint = int(epochFromDate)
        epochstr = str(epochint) + "000000000"

        amount = row[csvamount].replace(",", ".").replace("+", "")
        balance = row[csvbalance].replace(",", ".").replace("+", "")
        description1 = row[csvdescription].replace(" ", "\ ")


        if row['IBAN/BBAN'] == args.iban:
            databinary = "balance," + dbtag + "=" + str(row['IBAN/BBAN']) + ",description=\"" + description1 + " value=" + str(balance) + " " + str(epochstr)
            print(databinary)
            requests.post(url=protocol + "://" + domain + ":" + port + "/write?db=" + dbname, data=databinary)
